package com.devcamp.s50.task5820.restapi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.task5820.restapi.model.COrder;
import com.devcamp.s50.task5820.restapi.respository.IOrderRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class COrderController {
    @Autowired
    IOrderRepository pOrderRepository;

    @GetMapping("/orders")
    public ResponseEntity<List<COrder>> getAllOrders() {
        try {
            List<COrder> listOrders = new ArrayList<COrder>();
            pOrderRepository.findAll().forEach(listOrders::add);
            return new ResponseEntity<>(listOrders, HttpStatus.OK);

        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}

package com.devcamp.s50.task5820.restapi.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.s50.task5820.restapi.model.COrder;

public interface IOrderRepository extends JpaRepository<COrder, Long> {

}
